const desk = document.getElementById("desk");
const pointer = document.getElementById("pointer");
const eyeOpen = document.getElementById("eyes_open");
const tree = document.getElementById("tree");
const tree_1 = document.getElementById("tree_1");
const loading = document.getElementById("loading");
const withColor = document.querySelectorAll(".with-color");
const laptop = document.getElementById("laptop");

function scrollToTop(){
    document.querySelectorAll(".scroller").forEach(scroller => {scroller.scrollTop = 0});
}

function pointerColor(){
    withColor.forEach(elm => {
        elm.onmousemove = () => {pointer.style.borderColor = 'white';}
        elm.onmouseleave = () => {pointer.style.borderColor = '#5FB7CF';}
    })
}

setInterval(function(){
    eyeOpen.style.display = 'none';
    setTimeout(function(){eyeOpen.style.display = 'block';}, 100);
}, 5000);

document.querySelectorAll(".modal-action").forEach(btn => {
    const action = btn.dataset.action || "toggle"; // add | remove | toggle
    const {target} = btn.dataset;
    btn.onclick = () => {
        document.getElementById(target).classList?.[action]("active");
        setTimeout(()=>scrollToTop(), 500);
    } 
});

function removeTreeIntro(){
    setTimeout(function(){
        document.getElementById('tree_1').classList.remove('intro');
    }, 2000);
}

function consoleText(words, id, colors) {
  if (colors === undefined) colors = ['#403e41'];
  var visible = true;
  var con = document.getElementById('console');
  var letterCount = 1;
  var x = 1;
  var i = 0;
  var waiting = false;
  var target = document.getElementById(id)
  target.setAttribute('style', 'color:#403e41')
  var Intro = window.setInterval(function() {
    if(i < 4){
        if (letterCount === 0 && waiting === false) {
            i++;
            waiting = true;
            target.innerHTML = words[0].substring(0, letterCount)
            window.setTimeout(function() {
                var usedColor = colors.shift();
                colors.push(usedColor);
                var usedWord = words.shift();
                words.push(usedWord);
                x = 1;
                target.setAttribute('style', 'color:#403e41')
                letterCount += x;
                waiting = false;
            }, 1000)
        } else if (letterCount === words[0].length + 1 && waiting === false) {
            if(i === 3){
                clearInterval(Intro);
                setTimeout(() => {
                    desk.classList.add("active");
                    removeTreeIntro();
                }, 2500)
                setTimeout(() => {
                    document.getElementById("greeting").classList.add("hidden");
                }, 1000)
            }
            waiting = true;
            window.setTimeout(function() {
                x = -1;
                letterCount += x;
                letterCount = 4;
                waiting = false;
            }, 1000)
        } else if (waiting === false) {
            
            target.innerHTML = words[0].substring(0, letterCount)
            letterCount += x;
        }
    }
  }, 100)
  window.setInterval(function() {
    if (visible === true) {
      con.className = 'console-underscore hidden'
      visible = false;

    } else {
      con.className = 'console-underscore'

      visible = true;
    }
  }, 400);

    document.getElementById("btn_skip").onclick = () => {
        clearInterval(Intro);
        document.getElementById("greeting").classList.add("hidden");
        setTimeout(() => {
            desk.classList.add("active");
            removeTreeIntro();
        }, 1000)
    }
}

laptop.onclick = function(){
    this.classList.toggle("active");
}


document.getElementById("body").onload = function(){
    setTimeout(()=>{
        loading.classList.add('hidden');
    }, 1000);
    setTimeout(()=>{
        consoleText(
            ['Hi there...', 
            'I\'m Bhagas Nugroho.', 
            'Web Development define my main focus.',
            'One of my strengths in Web Development is Interactive Website.'
            ], 
            'text',
            ['tomato','rebeccapurple','lightblue']);
    }, 1500);
    pointerColor();
}
document.onmousemove = function(e){
    pointer.style.display = 'block'
    pointer.style.transform = `translate3D(${e.clientX-6}px, ${e.clientY-6}px, 0px)`;
}
